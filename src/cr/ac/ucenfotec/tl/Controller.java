package cr.ac.ucenfotec.tl;

import cr.ac.ucenfotec.bl.entities.carrera.Carrera;
import cr.ac.ucenfotec.bl.entities.estudiante.Estudiante;
import cr.ac.ucenfotec.bl.logic.CarreraGestor;
import cr.ac.ucenfotec.bl.logic.CursoGestor;
import cr.ac.ucenfotec.bl.logic.EstudianteGestor;
import cr.ac.ucenfotec.ui.UI;
import java.time.LocalDate;

public class Controller {

    private UI interfaz;
    private CarreraGestor carreraGestor;
    private CursoGestor cursoGestor;
    private EstudianteGestor estudianteGestor;

    public Controller(){
        interfaz = new UI();
        carreraGestor = new CarreraGestor();
        cursoGestor = new CursoGestor();
        estudianteGestor = new EstudianteGestor();
    }

    public void start() throws Exception{
        int opcion= -1;
        do{
            interfaz.mostrarMenu();
            opcion = interfaz.leerNumero();
            procesarOpcion(opcion);
        }while (opcion!=0);
    }

    public void procesarOpcion(int pOpcion) throws Exception{
        switch (pOpcion){
            case 1:
                registrarCarrera();
                break;
            case 2:
                listarCarreras();
                break;
            case 3:
                registrarCurso();
                break;
            case 4:
                listarCursos();
                break;
            case 5:
                asociarCurso();
                break;
            case 6:
                registrarEstudiante();
                break;
            case 7:
                listarEstudiantes();
                break;
            case 8:
                registrarGrupo();
                break;
            case 0:
                interfaz.imprimirMensaje("Gracias por usar el programa");
                break;
            default:
                interfaz.imprimirMensaje("Opción inválida");
                break;
        }
    }

    public void registrarCarrera() throws Exception{
        interfaz.imprimirMensaje("Digite el código de la carrera: ");
        String codigo = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite el nombre de la carrera: ");
        String nombre = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite si la carrera está acreditada (S) o no (N): ");
        boolean acreditada = Boolean.parseBoolean(interfaz.leerTexto().equals("S")?"True":"False");

        String mensaje = carreraGestor.registrarCarrera(codigo,nombre,acreditada);
        interfaz.imprimirMensaje(mensaje);
    }

    public void listarCarreras()throws Exception{
        for (Carrera carreraTemp:carreraGestor.getCarreras()) {
            System.out.println(carreraTemp);
        }
    }

    public void registrarCurso()throws Exception{
        interfaz.imprimirMensaje("Digite el código del curso: ");
        String codigo = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite el nombre del curso: ");
        String nombre = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite los créditos del curso: ");
        int credito = Integer.parseInt(interfaz.leerTexto());

        String mensaje = cursoGestor.registrarCurso(codigo,nombre,credito);
        interfaz.imprimirMensaje(mensaje);
    }

    public void listarCursos()throws Exception{
        cursoGestor.getCursos().forEach(cursoTemp -> interfaz.imprimirMensaje(cursoTemp.toString()));
    }

    public void asociarCurso()throws Exception{
        listarCursos();
        interfaz.imprimirMensaje("Digite el código del curso que desea asociar: ");
        String codigoCurso= interfaz.leerTexto();
        listarCarreras();
        interfaz.imprimirMensaje("Digite el código de la carrera a la que desea asociarle el curso: ");
        String codigoCarrera = interfaz.leerTexto();

        // falta conectarse con la lógica de negocio
        String mensaje = carreraGestor.asociarCurso(codigoCurso,codigoCarrera);
        interfaz.imprimirMensaje(mensaje);
    }

    public void registrarEstudiante() throws Exception{
        interfaz.imprimirMensaje("Digite la identificación del estudiante: ");
        String identificacion = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite el nombre del estudiante: ");
        String nombre= interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite el correo del estudiante: ");
        String correo= interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite la edad del estudiante: ");
        int edad = interfaz.leerNumero();
        interfaz.imprimirMensaje("Digite el año de nacimiento: ");
        int anio = interfaz.leerNumero();
        interfaz.imprimirMensaje("Digite el mes de nacimiento: ");
        int mes = interfaz.leerNumero();
        interfaz.imprimirMensaje("Digite el día de nacimiento: ");
        int dia = interfaz.leerNumero();
        LocalDate fechaNacimiento = LocalDate.of(anio,mes,dia);

        String mensaje = estudianteGestor.agregarEstudiante(identificacion,nombre,correo,edad,fechaNacimiento);
        interfaz.imprimirMensaje(mensaje);
    }

    public void listarEstudiantes() throws Exception{
        for (Estudiante estudiante:estudianteGestor.listarEstudiantes()) {
            interfaz.imprimirMensaje(estudiante.toString());
        }
    }

    public void registrarGrupo() throws Exception{
        listarCursos();
        interfaz.imprimirMensaje("Digite el código del curso: ");
        String codigoCurso = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite el código del grupo: ");
        String codigoGrupo = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite el número de grupo: ");
        int numero = interfaz.leerNumero();

        String mensaje = cursoGestor.registrarGrupo(codigoCurso,codigoGrupo,numero);
        interfaz.imprimirMensaje(mensaje);
    }

}
