package cr.ac.ucenfotec;

import cr.ac.ucenfotec.tl.Controller;

public class Main {

    public static void main(String[] args) throws Exception{
        Controller controlador = new Controller();
        controlador.start();
    }
}

