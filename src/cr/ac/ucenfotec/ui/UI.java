package cr.ac.ucenfotec.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class UI {

    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private PrintStream out = System.out;

    public void mostrarMenu(){
        out.println("1.Registrar carrera");
        out.println("2.Listar carreras");
        out.println("3.Registrar curso");
        out.println("4.Listar cursos");
        out.println("5.Asociar curso a una carrera.");
        out.println("6.Registrar Estudiante.");
        out.println("7.Listar Estudiantes.");
        out.println("8.Registrar Grupo.");
        out.println("0.Salir");
        out.print("Digite la opción que desea: ");
    }

    public int leerNumero() throws Exception{
        return Integer.parseInt(in.readLine());
    }

    public void imprimirMensaje(String mensaje){
        out.println(mensaje);
    }

    public String leerTexto() throws IOException{
        return in.readLine();
    }

}
